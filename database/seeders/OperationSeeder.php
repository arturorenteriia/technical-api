<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operations')->insert(
        ['type' => 'addition','cost' => 1]);

        DB::table('operations')->insert(
        ['type' => 'subtraction','cost' => 2]);

        DB::table('operations')->insert(
        ['type' => 'multiplication','cost' => 3]);

        DB::table('operations')->insert(
        ['type' => 'division','cost' => 4]);

        DB::table('operations')->insert(
        ['type' => 'square_root','cost' => 5]);

        DB::table('operations')->insert(
        ['type' => 'random_string','cost' => 6]);

    }
}
