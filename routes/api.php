<?php

use App\Http\Controllers\OperationController;
use App\Http\Controllers\UsersController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/tokens/create', function (Request $request) {

    $credentials = $request->only('email', 'password');

    $login = Auth::attempt($credentials);

    if($login){
        $token = User::where('email', $request->input('email'))->first()->createToken($request->email);
        return ['token' => $token->plainTextToken];
    }else{
        return [];
    }
    
});
Route::post('register', [UsersController::class, 'register']);

Route::middleware('auth:sanctum')->post('operation', [OperationController::class, 'operation']);
Route::middleware('auth:sanctum')->get('/logout', function (Request $request) {
    return $request->user()->tokens()->delete();
});

Route::middleware('auth:sanctum')->post('records', [OperationController::class, 'getRecords']);
Route::middleware('auth:sanctum')->post('delete-record', [OperationController::class, 'deleteRecord']);


