<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function register(Request $request){

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::create([
            'name' => 'test',
            'last_name' => 'test',
            'email' => $email,
            'password' => Hash::make($password),
            'active' =>true,
            'balance' =>50
        ]);
        return $this->success($user);
    }
}
