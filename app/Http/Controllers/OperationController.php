<?php

namespace App\Http\Controllers;

use App\Models\Operation;
use App\Models\Record;
use Illuminate\Http\Request;
use PHPUnit\Framework\Constraint\Operator;
use Symfony\Component\Console\Input\Input;

class OperationController extends Controller
{
    public function operation(Request $request){
       

        $operation = Operation::where('type', $request->input('operation'))->first();
        if($this->isUserAllowedToOperation($request->user(), $operation)){
            $number_1 = floatval($request->input('number1'));
            $number_2 = floatval($request->input('number2'));

            switch ($operation->type) {
                case 'addition':
                        $result = $number_1 + $number_2;
                    break;
                case 'subtraction':
                    $result = $number_1 - $number_2;
                    break;
                case 'multiplication':
                    $result = $number_1 * $number_2;
                    break;
                case 'division':
                    $result = $number_1 / $number_2;
                    break;
                case 'square_root':
                    $result = sqrt($number_1);
                    break;
                case 'random_string':
                    $result = $this->generateRandomString();
                    break;
                    default:
                        $result = 0;
                        break;
            }

        $new_record = new Record ();
    
        $new_record->operation_id = $operation->id;
        $new_record->amount = $operation->cost;
        $new_record->user_balance = $request->user()->balance - $operation->cost;
        $new_record->user_id = $request->user()->id;
        $new_record->operation_response = $result;

        $new_record->save();

        $request->user()->balance = $request->user()->balance -  $operation->cost;
        $request->user()->save();

        return $this->success($result);

        }

        return $this->fail([],'Your balance is to low, you need at least ' .$operation->cost . ' and you have '. $request->user()->balance);
    }

    public function isUserAllowedToOperation($user, $operation){

        return $user->balance >= $operation->cost ?? false;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function getRecords(Request $request) {
        
        $records = Record::with('operation')->where('user_id', $request->user()->id)->get();

        return $this->success($records);
    }

    function deleteRecord(Request $request) {
        
        $record_id = $request->input('record_id');

        $record = Record::find($record_id);

        if($record){
            $record->delete();
        }

        return $this->success(Record::with('operation')->where('user_id', $request->user()->id)->get());
    }
}
